package com.eciformacion.ejercicio4.gestor;

public class NoHaySuficientesCamisetasException extends Exception {
	public NoHaySuficientesCamisetasException(String string) {
		super(string);
	}
}
