package com.eciformacion.ejercicio4.gestor;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
//testing git
import java.util.*;
import java.util.Map.Entry;

public class GestorStockCamiseta {
	private HashMap<String,Integer> stock;
	private Connection conn;

	public GestorStockCamiseta(Connection conn) {
		this.stock = new HashMap<String, Integer>();
		this.conn = conn;
		try {
		Statement st = this.conn.createStatement();
		ResultSet rs = st.executeQuery("SELECT * FROM almacen");
		while(rs.next()) {
			this.stock.put(rs.getString(1),rs.getInt(2));
		}
		} catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	public void aņadirCamiseta(String ref, int cantidad) {
		if (!this.stock.containsKey(ref)) {
			this.stock.put(ref, cantidad);
			//aņadir a la base de datos
			System.out.println("No hay");
			try {
			PreparedStatement ps =this.conn.prepareStatement("INSERT INTO almacen values(?,?)");
			ps.setString(1, ref);
			ps.setInt(2, cantidad);
			ps.executeUpdate();
			}catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			this.stock.put(ref, this.stock.get(ref)+cantidad);
			//actualizar la base de datos
			try {
				PreparedStatement ps =this.conn.prepareStatement("UPDATE almacen SET modelo=?,cantidad=? WHERE modelo=?");
				ps.setString(1, ref);
				ps.setInt(2, this.stock.get(ref));
				ps.setString(3, ref);
				ps.executeUpdate();
				}catch (Exception e) {
					e.printStackTrace();
				}
		}
	}
	
	public void retirarCamiseta(String ref, int cantidad) throws NoHayCamisetasException, NoHaySuficientesCamisetasException {
		if (!this.stock.containsKey(ref)) {
			throw new NoHayCamisetasException(ref);
		} else if(this.stock.get(ref) < cantidad){
			throw new NoHaySuficientesCamisetasException(ref+" "+this.stock.get(ref));
		} else {
			//actualizar la base de datos
			this.stock.put(ref, this.stock.get(ref)-cantidad);
			try {
				PreparedStatement ps =this.conn.prepareStatement("UPDATE almacen SET modelo=?,cantidad=? WHERE modelo=?");
				ps.setString(1, ref);
				ps.setInt(2, this.stock.get(ref));
				ps.setString(3, ref);
				ps.executeUpdate();
				}catch (Exception e) {
					e.printStackTrace();
				}
			if(this.stock.get(ref) <= 0) {
				this.stock.remove(ref);
				try {
					PreparedStatement ps =this.conn.prepareStatement("DELETE FROM `almacen` WHERE modelo=?");
					ps.setString(1, ref);
					ps.executeUpdate();
					}catch (Exception e) {
						e.printStackTrace();
					}
			}
		}
	}
	
	public Map<String, Integer> buscarCamiseta(String iniref) {
		HashMap<String, Integer> camisetas = new HashMap<>();
		//System.out.println("hola");
		this.stock.entrySet().stream().filter(p -> p.getKey().startsWith(iniref)).forEach(p -> {
			camisetas.put(p.getKey(), p.getValue());
		});
		/*for (Iterator iterator = this.stock.entrySet().iterator(); iterator.hasNext();) {
			Entry<String, Integer> e = (Entry) iterator.next();
			if(e.getKey().startsWith(iniref)) camisetas.put(e.getKey(), e.getValue());
			
		}*/
		return camisetas;
	}
	
	public HashMap<String, Integer> buscarCamisetaMaxStock(int limite){
		HashMap<String, Integer> camisetas = new HashMap<>();
		this.stock.entrySet().stream().filter(p -> p.getValue() > limite).forEach(p -> {
			camisetas.put(p.getKey(), p.getValue());
		});
		/*for (Iterator iterator = this.stock.entrySet().iterator(); iterator.hasNext();) {
			Entry<String, Integer> e = (Entry) iterator.next();
			if(e.getValue() > limite) camisetas.put(e.getKey(), e.getValue());
					
		}		*/
		return camisetas;
	}
	
	public HashMap<String, Integer> buscarCamisetaMinStock(int limite){
		HashMap<String, Integer> camisetas = new HashMap<>();
		this.stock.entrySet().stream().filter(p -> p.getValue() < limite).forEach(p -> {
			camisetas.put(p.getKey(), p.getValue());
		});	
		return camisetas;
	}
	
	public HashMap<String,Integer> getStock() {
		return stock;
	}

	public void setStock(HashMap<String,Integer> stock) {
		this.stock = stock;
	}
	//Comentario para master
	///comentario test Excepciones
	public static void main(String[] args) {
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return;
		}
		Connection conn = null;
		try {
		conn=DriverManager.getConnection("jdbc:mysql://localhost/camisetas", "root", "");
		
		GestorStockCamiseta stock = new GestorStockCamiseta(conn);
		stock.aņadirCamiseta("hola", 5);
		stock.aņadirCamiseta("hola", 3);
		stock.aņadirCamiseta("holamundo", 7);
		stock.aņadirCamiseta("holaquetal", 2);
		stock.aņadirCamiseta("fernandoquetal", 5);
		stock.aņadirCamiseta("fernandtal", 5);
		stock.aņadirCamiseta("fandoquetal", 8);
		System.out.println(stock.buscarCamiseta("hola"));
		System.out.println(stock.buscarCamisetaMaxStock(4));
		System.out.println(stock.buscarCamisetaMinStock(8));
		try {
			stock.retirarCamiseta("hola", 4);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			stock.retirarCamiseta("pedro", 8);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
		stock.retirarCamiseta("fandoquetal", 11);
		}catch (Exception e){
			e.printStackTrace();
		}
		System.out.println("/////");
		System.out.println(stock.buscarCamiseta("hola"));
		
		conn.close();
	}catch(Exception e) {
		System.out.println("Error conexion");
		e.printStackTrace();
		return;
	}
		try {
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public Connection getConn() {
		return conn;
	}

	public void setConn(Connection conn) {
		this.conn = conn;
	}
}
